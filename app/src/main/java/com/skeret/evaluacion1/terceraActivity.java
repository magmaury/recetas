package com.skeret.evaluacion1;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class terceraActivity extends AppCompatActivity {

    private EditText etUsuario, etPass;
    private Button btRegistrarse;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tercera);

        this.etUsuario = findViewById(R.id.etUsuario);
        this.etPass = findViewById(R.id.etPass);
        this.btRegistrarse = findViewById(R.id.btRegistrarse);

        this.btRegistrarse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String usuario = etUsuario.getText().toString().trim();
                String pass = etPass.getText().toString();
                if(usuario.equals("")){
                    Toast.makeText(getApplicationContext(), "Ingrese un usuario", Toast.LENGTH_SHORT).show();
                }
                else if(pass.equals("")){
                    Toast.makeText(getApplicationContext(), "Ingrese una contraseña", Toast.LENGTH_SHORT).show();
                }
                else{
                    Toast.makeText(getApplicationContext(), "Su contraseña es "+pass, Toast.LENGTH_LONG).show();
                }
            }
        });
    }
}

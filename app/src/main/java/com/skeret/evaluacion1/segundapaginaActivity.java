package com.skeret.evaluacion1;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class segundapaginaActivity extends AppCompatActivity {

    private EditText etEmail, etTelefono;
    private Button btSiguiente2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_segundapagina);

        this.etEmail = findViewById(R.id.etEmail);
        this.etTelefono = findViewById(R.id.etTelefono);
        this.btSiguiente2 = findViewById(R.id.btRegistrarse);

        this.btSiguiente2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String email = etEmail.getText().toString().trim();
                if(email.equals("")){
                    Toast.makeText(getApplicationContext(), "Ingrese su correo electrónico", Toast.LENGTH_SHORT).show();
                }
                else{
                    Intent pasarVentana = new Intent(segundapaginaActivity.this, terceraActivity.class);
                    startActivity(pasarVentana);
                }
            }
        });
    }
}

package com.skeret.evaluacion1;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {


    private EditText etNombre, etEdad;
    private Button btSiguiente1;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        this.etNombre = findViewById(R.id.etNombre);
        this.etEdad = findViewById(R.id.etEdad);
        this.btSiguiente1 = findViewById(R.id.btRegistrarse);

        this.btSiguiente1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String nombre = etNombre.getText().toString().trim();
                if(nombre.equals("")){
                    Toast.makeText(getApplicationContext(), "Ingrese su nombre", Toast.LENGTH_SHORT).show();
                }
                else{
                    Intent pasarVentana = new Intent(MainActivity.this, segundapaginaActivity.class);
                    startActivity(pasarVentana);
                }
            }
        });

    }
}
